
const passenger = require('express').Router();
const axios = require("axios");
const passengerbyid = require("./getpassengerbyid")

passenger.get("/passengerone", passengerbyid)
passenger.get("/get", async (req, res) => {
	try {
		const response = await axios.get("https://api.instantwebtools.net/v1/passenger?page=0&size=10");
        console.log(response.data)
		res.status(200).json(response.data);
	} catch (err) {
		res.status(500).json({ message: err });
	}
});

passenger.get("/post", async (req, res) => {
	try {
		const response = await axios.post("https://api.instantwebtools.net/v1/passenger", 
        {
            "name": "John Doe",
            "trips": 250,
            "airline": 5
        },
        {
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                "Access-Control-Allow-Origin": "*",
            }
        });
		res.status(200).json(response.data);
	} catch (err) {
		res.status(500).json({ message: err });
	}
});

module.exports = passenger;