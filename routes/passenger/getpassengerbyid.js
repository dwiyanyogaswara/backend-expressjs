const axios = require("axios");

//controller function 
module.exports = async (req, res) => {
	try {
		const response = await axios.get("https://api.instantwebtools.net/v1/passenger/60002f44a8623a55ce4fa3d7");
        console.log(response.data)
		res.status(200).json(response.data);
	} catch (err) {
		res.status(500).json({ message: err });
	}
};