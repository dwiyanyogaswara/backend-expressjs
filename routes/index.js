const routes = require('express').Router();
const passenger = require('./passenger');
const mysql = require("./mysql")

routes.use('/passenger', passenger);

routes.get('/', (req, res) => {
  res.status(200).json({ message: 'Connected!' });
});

routes.use('/mysql', mysql)
module.exports = routes;