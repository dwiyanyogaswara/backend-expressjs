const express = require('express')
const app = express()
const port = 60001

// app.get('/', (req, res) => {
//     res.send('Hello World!')
// })

const routes = require('./routes');

//  Connect all our routes to our application
app.use('/', routes);

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})